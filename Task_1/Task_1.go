package main

import "fmt"

type Human struct {
	name string
	age  int
	city string
}

type Action struct {
	*Human
}

func main() {
	personAction := &Action{
		&Human{name: "Vera Pavlova", age: 32, city: "Москва"},
	}

	fmt.Printf("Before: name %s, age %d, city %s\n", personAction.name, personAction.age, personAction.city)
	personAction.changeName("Olga Grigorieva")
	personAction.changeAge(28)
	personAction.changeCity("Томск")
	fmt.Printf("After: name %s, age %d, city %s", personAction.name, personAction.age, personAction.city)
}

func (a *Action) changeName(change string) {
	a.name = change
}

func (a *Action) changeAge(change int) {
	a.age = change
}

func (a *Action) changeCity(change string) {
	a.city = change
}
