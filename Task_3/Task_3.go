package main

import (
	"fmt"
	"sync"
)

func main() {
	numbers := []int{2, 4, 6, 8, 10}

	// Создаем канал для передачи результатов из горутин в основную программу.
	resultChannel := make(chan int)

	// Используем WaitGroup для ожидания завершения всех горутин.
	var wg sync.WaitGroup

	// Запускаем горутины для каждого числа.
	for _, num := range numbers {
		wg.Add(1)
		go squareWorker(num, resultChannel, &wg)
	}

	// Запускаем горуту для закрытия канала после завершения всех горутин.
	go func() {
		wg.Wait()
		close(resultChannel)
	}()

	// Считаем сумму квадратов из канала и выводим результат.
	sum := 0
	for result := range resultChannel {
		sum += result
	}

	fmt.Printf("Сумма квадратов: %d\n", sum)
}

// squareWorker рассчитывает квадрат числа и отправляет результат в канал.
func squareWorker(number int, resultChannel chan<- int, wg *sync.WaitGroup) {
	defer wg.Done()
	result := number * number
	resultChannel <- result
}
